/* Info :
 Queen Chiken Level: 20 - 99 
 Player :  5 - 15 :
 Delay : 60 min. 
 Reputation : +100 
 Gold : 1000 - 2000
 Data : 
 Damage% : yes
 TeleportAll : yes
 OneSeal : yes
 */
DECLARE @BoxId SMALLINT = 302
DECLARE @BoxDesign SMALLINT = 11


INSERT INTO [dbo].[RollGeneratedItem] 
(
	[OriginalItemDesign], [OriginalItemVNum],
	[MinimumOriginalItemRare], [MaximumOriginalItemRare], [ItemGeneratedVNum],
	[ItemGeneratedDesign],	[ItemGeneratedAmount], [IsRareRandom],
	[Probability]
)
VALUES

	(@BoxDesign, @BoxId, '0', '7', '5259', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2404', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '5262', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '5107', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '5108', '0', '2', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4151', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '9357', '0', '1', '0', '1')