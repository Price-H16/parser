/* Info :
 Draco Level: 80 - 99 
 Player :  5 - 15 :
 Delay : 60 min. 
 Reputation : +2400 
 Gold : 20000
 Data : 
 Damage% : no
 TeleportAll : no
 OneSeal : yes
 */
DECLARE @BoxId SMALLINT = 302
DECLARE @BoxDesign SMALLINT = 17


INSERT INTO [dbo].[RollGeneratedItem] 
(
	[OriginalItemDesign], [OriginalItemVNum],
	[MinimumOriginalItemRare], [MaximumOriginalItemRare], [ItemGeneratedVNum],
	[ItemGeneratedDesign],	[ItemGeneratedAmount], [IsRareRandom],
	[Probability]
)
VALUES

	(@BoxDesign, @BoxId, '0', '7', '4498', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4499', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4485', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2513', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2282', '0', '6', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '1030', '0', '3', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '1249', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2349', '0', '2', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2349', '0', '3', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2515', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2517', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2519', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '9373', '0', '1', '0', '1')