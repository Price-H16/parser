/* Info :
 Draco Level: 75 - 99 
 Player :  5 - 15 :
 Delay : 60 min. 
 Reputation : +2250 
 Gold : 20000
 Data : 
 Damage% : no
 TeleportAll : no
 OneSeal : yes
 */
DECLARE @BoxId SMALLINT = 302
DECLARE @BoxDesign SMALLINT = 16


INSERT INTO [dbo].[RollGeneratedItem] 
(
	[OriginalItemDesign], [OriginalItemVNum],
	[MinimumOriginalItemRare], [MaximumOriginalItemRare], [ItemGeneratedVNum],
	[ItemGeneratedDesign],	[ItemGeneratedAmount], [IsRareRandom],
	[Probability]
)
VALUES

	(@BoxDesign, @BoxId, '0', '7', '4500', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4501', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4502', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4486', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2511', '0', '3', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2512', '0', '2', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2513', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2514', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2516', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2518', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2282', '0', '6', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '1030', '0', '3', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '1249', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '9372', '0', '1', '0', '1')