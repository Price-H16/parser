/* Info :
 Snowman Level: 20 - 99 
 Player :  5 - 15 :
 Delay : 60 min. 
 Reputation : +100 
 Gold : 1000 - 2000
 Data : 
 Damage% : yes
 TeleportAll : yes
 OneSeal : no
 */
DECLARE @BoxId SMALLINT = 302
DECLARE @BoxDesign SMALLINT = 8


INSERT INTO [dbo].[RollGeneratedItem] 
(
	[OriginalItemDesign], [OriginalItemVNum],
	[MinimumOriginalItemRare], [MaximumOriginalItemRare], [ItemGeneratedVNum],
	[ItemGeneratedDesign],	[ItemGeneratedAmount], [IsRareRandom],
	[Probability]
)
VALUES

	(@BoxDesign, @BoxId, '0', '7', '4800', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4802', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4803', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4804', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4805', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4806', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '5932', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '974', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '975', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4076', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '977', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '978', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4075', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4067', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4068', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4069', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4070', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4071', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4072', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '1369', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '5207', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2328', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '445', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '5970', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '5213', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '3113', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '3114', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '1581', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4406', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '9365', '0', '1', '0', '1')