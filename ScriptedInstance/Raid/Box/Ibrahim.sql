/* Info :
 Ibrahim Level: 80 - 99 
 Player :  5 - 15 :
 Delay : 60 min. 
 Reputation : +2400 
 Gold : 19000 - 20000
 Data : 
 Damage% : no
 TeleportAll : no
 OneSeal : yes
 */
DECLARE @BoxId SMALLINT = 302
DECLARE @BoxDesign SMALLINT = 9
DECLARE @BagId SMALLINT = 1777
DECLARE @BagDesign SMALLINT = 0


INSERT INTO [dbo].[RollGeneratedItem] 
(
	[OriginalItemDesign], [OriginalItemVNum],
	[MinimumOriginalItemRare], [MaximumOriginalItemRare], [ItemGeneratedVNum],
	[ItemGeneratedDesign],	[ItemGeneratedAmount], [IsRareRandom],
	[Probability]
)
VALUES

	(@BoxDesign, @BoxId, '0', '7', '4001', '0', '1', '0', '2'),/* Épée tranchante du grand chef */
	(@BoxDesign, @BoxId, '0', '7', '4003', '0', '1', '0', '2'),/* Arc du grand chef */
	(@BoxDesign, @BoxId, '0', '7', '4005', '0', '1', '0', '2'),/* Baguette magique du grand chef */
	(@BoxDesign, @BoxId, '0', '7', '4007', '0', '1', '0', '2'),/* Arbalète du grand chef */
	(@BoxDesign, @BoxId, '0', '7', '4009', '0', '1', '0', '2'),/* Couteau du grand chef */
	(@BoxDesign, @BoxId, '0', '7', '4011', '0', '1', '0', '2'),/* Arme enchantée du grand chef */
	(@BoxDesign, @BoxId, '0', '7', '4013', '0', '1', '0', '2'),/* Armure lourde du commandant */
	(@BoxDesign, @BoxId, '0', '7', '4016', '0', '1', '0', '2'),/* Armure en cuir du commandant */
	(@BoxDesign, @BoxId, '0', '7', '4019', '0', '1', '0', '2'),/* Apparat du commandant*/
	(@BoxDesign, @BoxId, '0', '7', '1873', '0', '1', '0', '2'),/* Pièce d'argent de la bande des voleurs */
	(@BoxDesign, @BoxId, '0', '7', '1873', '0', '2', '0', '2'),/* Pièce d'argent de la bande des voleurs */
	(@BoxDesign, @BoxId, '0', '7', '1873', '0', '3', '0', '2'),/* Pièce d'argent de la bande des voleurs */
	(@BagDesign, @BagId, '0', '7', '1876', '0', '3', '0', '2'),/* Clé en or d'Ibrahim */
	(@BagDesign, @BagId, '0', '7', '1872', '0', '3', '0', '2'),/* Pièce d'or de la bande des voleurs */
	(@BagDesign, @BagId, '0', '7', '2349', '0', '3', '0', '2'),/* Pierre précieuse brillante bleu ciel */
	(@BagDesign, @BagId, '0', '7', '9366', '0', '3', '0', '1')/* Voleur expert */